# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends KinematicBody2D

const OSC_PERIOD = 2.0
const OSC_AMPLITUDE = 3.0
var _osc_timer: float = 0
var vel: Vector2 = Vector2()

func _process(delta):
	z_index = int(position.y)

func _physics_process(delta):
	_osc_timer = fmod(_osc_timer + delta, OSC_PERIOD)
	$Bar.position.y = sin(_osc_timer / OSC_PERIOD * PI * 2.0) * OSC_AMPLITUDE
	var collision: KinematicCollision2D = move_and_collide(vel, true, true, true)
	if collision != null:
		vel = collision.travel
	position += vel
	vel *= 1 - delta * 6
	for area in $Area.get_overlapping_areas():
		_on_Area_entered(area)

func _on_Area_entered(area):
	if not area.has_meta("body"):
		return
	var body = area.get_meta("body")
	if not body.has_method("give"):
		return
	if not ActorFuncs.within_depth_hitbox(self, body):
		return
	var value = 10
	body.give({"gp": value})
	var pickup_text = preload("res://actors/GPPickupText.tscn").instance()
	pickup_text.value = value
	pickup_text.position = position + Vector2(0, - 38)
	get_parent().add_child(pickup_text)
	get_tree().call_group("world", "delete_mob", self)
