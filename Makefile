
PKGNAME=ksi
ITCHNAME=killer-snowman-invasion

.PHONY: help deploy deploy-linux64 deploy-win64 deploy-win32 deploy-macos

help:
	@echo "This makefile can be used to deploy to itch.io. Make sure you export the project"
	@echo "from Godot first."
	@echo
	@echo "  Supported Command            Folder/File"
	@echo "  make deploy-linux64          export/linux64"
	@echo "  make deploy-win64            export/win64"
	@echo "  make deploy-win32            export/win32"
	@echo "  make deploy-macos            export/$(PKGNAME).macos.zip"
	@echo "  make deploy                  All of the above"

export/$(PKGNAME).linux64.zip: export/linux64/$(PKGNAME).x86_64 export/linux64/$(PKGNAME).pck
	zip -j -u $@ $^

export/$(PKGNAME).win64.zip: export/win64/$(PKGNAME).exe export/win64/$(PKGNAME).pck
	x86_64-w64-mingw32-strip export/win64/$(PKGNAME).exe
	zip -j -u $@ $^

export/$(PKGNAME).win32.zip: export/win32/$(PKGNAME).exe export/win32/$(PKGNAME).pck
	i686-w64-mingw32-strip export/win32/$(PKGNAME).exe
	zip -j -u $@ $^

deploy: deploy-linux64 deploy-win64 deploy-win32 deploy-macos

deploy-linux64: export/$(PKGNAME).linux64.zip
	butler push $^ meditator/$(ITCHNAME):linux-64

deploy-win64: export/$(PKGNAME).win64.zip
	butler push $^ meditator/$(ITCHNAME):win-64

deploy-win32: export/$(PKGNAME).win32.zip
	butler push $^ meditator/$(ITCHNAME):win-32

deploy-macos: export/$(PKGNAME).macos.zip
	butler push $^ meditator/$(ITCHNAME):macos
