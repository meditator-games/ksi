# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Node

static func speak(actor: Node2D, text: String, size: Vector2 = Vector2(-1, -1)):
	var bubble
	if actor.has_meta("bubble"):
		bubble = actor.get_meta("bubble")
	else:
		bubble = preload("res://actors/DialogBubble.tscn").instance()
		actor.get_parent().add_child(bubble)
	
	if size.x != -1 and size.y != -1:
		bubble.set_size(size)
	bubble.say(text, actor)
	return bubble

static func set_anim(parts: Array, anim: String):
	for part in parts:
		if part is AnimatedSprite and part.frames.has_animation(anim):
			part.animation = anim

static func blink(actor: Node2D, head: AnimatedSprite, delta: float):
	match head.animation:
		"Blink":
			actor.blink_timer -= delta
			if actor.blink_timer < 0:
				head.animation = "Normal"
				actor.blink_timer += randf() * 3.0 + 2.0
		_:
			head.animation = "Normal"
			actor.blink_timer -= delta
			if actor.blink_timer < 0:
				head.animation = "Blink"
				actor.blink_timer += 0.2

static func within_depth_hitbox(a: Node2D, b: Node2D):
	var ydiff = a.position.y - b.position.y
	return ydiff >= -b.HITBOX_DEPTH and ydiff <= b.HITBOX_DEPTH
	
static func get_attack_body(source: Node2D, target: Area2D) -> Node2D:
	if not target.has_meta("body"):
		return null
	var body = target.get_meta("body")
	
	if source == body:
		return null
	# don't allow projectiles to damage the actor who threw them
	if source.has_meta("owner_path") and source.get_meta("owner_path") == body.get_path():
		return null
	if not within_depth_hitbox(source, body):
		return null
	if not body.has_method("damaged_melee"):
		return null
	return body
	
static func melee_attack(source: Node2D, target: Area2D, thrust: float, damage: int) -> Node2D:
	var body = get_attack_body(source, target)
	if body != null:
		var diff = source.position - body.position
		body.damaged_melee(source, Vector3(diff.x, diff.y, 0.0).normalized() * -thrust, damage)
	return body
	
static func find_target(world: Node2D, aggressiveness: float = 0.0) -> Node2D:
	var search = []
	if world.player != null and not world.player.is_dead():
		if aggressiveness >= 1.0:
			return world.player
		search.append(world.player)
	for mob in world.mobs:
		if mob is preload("res://scripts/Dummy.gd"):
			search.append(mob)
	if not search.empty():
		return search[randi() % search.size()]
	return null
	
static func spawn_gold(tree: SceneTree, position: Vector2, count: int):
	for i in range(count):
		var gold = preload("res://actors/Gold.tscn").instance()
		gold.position = position + Vector2(rand_range(-10, 10), rand_range(5, 5))
		tree.call_group("world", "add_mob", gold)
	
static func oneshot_particles(particles: Particles2D, parent: Node):
	if particles.is_inside_tree():
		var pos = particles.global_position
		particles.get_parent().remove_child(particles)
		particles.position = pos
	parent.add_child(particles)
	particles.emitting = true
	particles.restart()
	var timer = Timer.new()
	particles.add_child(timer)
	timer.start(particles.lifetime)
	timer.connect("timeout", particles, "queue_free")