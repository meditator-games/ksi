# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Control

func _ready():
	get_tree().connect("screen_resized", self, "_center")
	Global.center_viewport($Display)
	Global.center_viewport($Display)
	Settings.update_current_viewport()

func _center():
	Global.center_viewport($Display)
	Global.center_viewport($HUD)

func setup(world_seed: int, player_params: Dictionary):
	$Viewport/World.world_seed = float(world_seed)
	$Viewport/World.create_player(player_params, $HUDVP/HUD)