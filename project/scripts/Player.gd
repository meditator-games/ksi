# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends "res://scripts/Dummy.gd"

const MOVE_SPEED_Y = 40.0
const MOVE_SPEED_X = 60.0
const RUN_FACTOR = 2.0
const PUNCH_DURATION = 0.2
const JUMP_STRENGTH = 200.0
const DOUBLE_TAP_WINDOW = 0.2
const INIT_HP = 8
const INVIS_DURATION = 1.0

onready var body_init_pos = $Body.position
var xmove: Array = []
var ymove: Array = []
var running: bool = false
var double_tap_dir: String = ""
var double_tap_timer: float = 0
var double_tap_cooldown: float = 0
var respawn_timer: float = 0
var invis_timer: float = 0.0
var frozen: bool = false
var hardcore: bool = false
var hud: Control = null
var respawn_point: Vector2 = Vector2()
var caret
var sounds

var hp: int = INIT_HP
var max_hp: int = INIT_HP
var gp: int = 0
var strength: int = 1
var dexterity: int = 1
var constitution: int = 1

func _play_sound(name: String):
	sounds.get_node(name).play()

func _ready():
	for i in range(12):
		var roll = randi() % 4
		if roll == 0:
			strength += 1
		elif roll == 1:
			dexterity += 1
		elif roll == 2:
			constitution += 1
		elif roll == 3:
			max_hp += 1
			hp += 1
	$Body/AreaFist.connect("area_entered", self, "_punch")
	respawn_point = position
	
	caret = preload("res://ui/Caret.tscn").instance()
	caret.position = $Body/TopPoint.position
	$Body.add_child(caret)
	
	sounds = preload("res://sfx/PlayerSounds.tscn").instance()
	$Body.add_child(sounds)
	
	call_deferred("refresh_hud")
	
func _punch(obj):
	var hit = ActorFuncs.melee_attack(self, obj, PUNCH_THRUST, strength)
	if hit != null:
		_play_sound("Punch")
		if not (hit is preload("res://scripts/Dummy.gd")):
			var particles = preload("res://actors/SnowHitParticle.tscn").instance()
			particles.position = hit.position + Vector2(0, -30)
			var direction = (hit.position - position).normalized()
			particles.process_material.set_shader_param("initial_thrust", direction * clamp(strength, 1, 20) * PUNCH_THRUST * 10.0)
			ActorFuncs.oneshot_particles(particles, get_parent())

func refresh_hud():
	if hud != null:
		hud.set_hp(hp, max_hp)
		hud.set_gp(gp)
		hud.set_strength(strength)
		hud.set_dexterity(dexterity)
		hud.set_constitution(constitution)
	
func damaged_melee(source: Node2D, dir: Vector3, value: int):
	if hurt_timer > 0 or invis_timer > 0 or hp <= 0:
		return
	if randi() % (100 - constitution) == 0:
		return

	hp -= value
	if hud != null:
		hud.set_hp(hp, max_hp)
	if hp <= 0:
		state = DummyState.DEAD
		respawn_timer = 1.0
		double_tap_timer = 0
		double_tap_cooldown = 0
		running = false
		ymove = []
		xmove = []
		$Body.rotation_degrees = -90
		$Body.position = Vector2(21, 6)
		caret.visible = false
	else:
		hurt_timer = 0.5
	thrust_vel = dir

func enter_building(building):
	if frozen or state == DummyState.DEAD:
		return
	frozen = true
	_play_sound("Door")
	respawn_point = position + Vector2(0, 16)
	get_parent().fade_out_music()
	Global.set_fade_color(Color(0.0, 0.0, 0.0, 1.0))
	Global.fade_to(get_parent(), "enter_building", [building])

func spend(cost: int) -> bool:
	if gp >= cost:
		gp -= cost
		if hud != null:
			hud.set_gp(gp)
		return true
	return false

func give(powerups: Dictionary):
	for type in powerups.keys():
		match type:
			"hp":
				_play_sound("Chew")
				hp = min(hp + powerups[type], max_hp)
				if hud != null:
					hud.set_hp(hp, max_hp)
			"max_hp":
				_play_sound("Powerup")
				max_hp += powerups[type]
				hp += powerups[type]
				if hud != null:
					hud.set_hp(hp, max_hp)
			"gp":
				_play_sound("Coin")
				gp += powerups[type]
				get_parent().total_gp += powerups[type]
				if hud != null:
					hud.set_gp(gp)
			"strength":
				_play_sound("Powerup")
				strength = clamp(strength + powerups[type], 1, 99)
				if hud != null:
					hud.set_strength(strength)
			"dexterity":
				_play_sound("Powerup")
				dexterity = clamp(dexterity + powerups[type], 1, 99)
				if hud != null:
					hud.set_dexterity(dexterity)
			"constitution":
				_play_sound("Powerup")
				constitution = clamp(constitution + powerups[type], 1, 99)
				if hud != null:
					hud.set_constitution(constitution)
			"win":
				_play_sound("Powerup")
				Global.set_fade_color(Color(1.0, 1.0, 1.0, 1.0))
				Global.fade_to(Global, "victory", [get_parent().get_stats()])
				

func _process(delta):
	if invis_timer > 0:
		$Body.visible = !$Body.visible
	else:
		$Body.visible = true
	
func _respawn():
	hp = max_hp
	vel = Vector3()
	state = DummyState.STAND
	position = respawn_point
	$Body.rotation_degrees = 0
	$Body.position = body_init_pos
	caret.visible = true
	invis_timer = INVIS_DURATION
	gp = 0
	if hud != null:
		hud.set_hp(hp, max_hp)
		hud.set_gp(gp)

func _physics_process(delta):
	if state == DummyState.DEAD:
		if respawn_timer > 0:
			respawn_timer -= delta
		else:
			for action in ["jump", "attack", "ui_accept", "ui_select", "ui_cancel"]:
				if Input.is_action_just_pressed(action):
					if hardcore:
						Global.fade_to(Global, "quit_to_title")
					else:
						_respawn()
		return
		
	if invis_timer > 0:
		invis_timer -= delta
	if hurt_timer > 0  and hurt_timer - delta <= 0:
		invis_timer = INVIS_DURATION
	if double_tap_timer > 0:
		double_tap_timer -= delta
	var step_sound = sounds.get_node("Step")
	step_sound.playing = state == DummyState.WALK
		
	if frozen:
		step_sound.playing = false
		xmove = []
		ymove = []
		return
		
	if Input.is_action_just_pressed("move_up"):
		if double_tap_dir == "up" and double_tap_timer > 0:
			running = true
		ymove.append(-1)
		double_tap_dir = "up"
		double_tap_timer = DOUBLE_TAP_WINDOW
	if Input.is_action_just_released("move_up"):
		while ymove.has(-1):
			ymove.erase(-1)
			
	if Input.is_action_just_pressed("move_down"):
		if double_tap_dir == "down" and double_tap_timer > 0:
			running = true
		ymove.append(1)
		double_tap_dir = "down"
		double_tap_timer = DOUBLE_TAP_WINDOW
	if Input.is_action_just_released("move_down"):
		while ymove.has(1):
			ymove.erase(1)
			
	if Input.is_action_just_pressed("move_left"):
		if double_tap_dir == "left" and double_tap_timer > 0:
			running = true
		xmove.append(-1)
		double_tap_dir = "left"
		double_tap_timer = DOUBLE_TAP_WINDOW
	if Input.is_action_just_released("move_left"):
		while xmove.has(-1):
			xmove.erase(-1)
			
	if Input.is_action_just_pressed("move_right"):
		if double_tap_dir == "right" and double_tap_timer > 0:
			running = true
		xmove.append(1)
		double_tap_dir = "right"
		double_tap_timer = DOUBLE_TAP_WINDOW
	if Input.is_action_just_released("move_right"):
		while xmove.has(1):
			xmove.erase(1)
			
	if Input.is_action_just_pressed("attack"):
		punch_timer = PUNCH_DURATION
		punch_init = true
		
	if Input.is_action_just_pressed("jump") and $Body.position.y == 0:
		vel.z = -JUMP_STRENGTH * delta
		_play_sound("Jump")
	
	var dir = Vector2()
	if not ymove.empty():
		dir.y = ymove[0]
	if not xmove.empty():
		dir.x = xmove[0]
		
	if xmove.empty() and ymove.empty() and running:
		if double_tap_cooldown > 0:
			double_tap_cooldown -= delta
			if double_tap_cooldown <= 0:
				running = false
		else:
			double_tap_cooldown = DOUBLE_TAP_WINDOW
	dir *= RUN_FACTOR if running else 1
	
	if dir.x < 0:
		scale.x = -1
	elif dir.x > 0:
		scale.x = 1
	
	var move = dir * Vector2(MOVE_SPEED_X, MOVE_SPEED_Y) * delta * ((dexterity + 19) / 20.0)
	vel.x = move.x
	vel.y = move.y
	step_sound.pitch_scale = 0.5 + clamp(move.length() * 0.1, 0.0, 2.0)
	
		