# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends "res://scripts/Dummy.gd"

const MOVE_SPEED_Y = 40.0
const MOVE_SPEED_X = 60.0

enum AIState {
	WALK, WANDER
}

var _ai_state = AIState.WANDER
var _move_time: float = 0
var _building_timer: float = 2.0 # don't allow entering buildings until a few secs after spawning
var _dodge_vec: Vector2 = Vector2()
var _move_vec: Vector2 = Vector2()
var _move_speed: float = 1.0
var _fleeing = false
var _harmed = false

func _ready():
	connect("body_collided", self, "_collided")

func _collided(collision):
	match _ai_state:
		AIState.WALK:
			if _move_time <= 0:
				_dodge_vec = Vector2(0, -1 if randi() % 2 == 0 else 1)
				_move_time = rand_range(0.2, 0.4)
		AIState.WANDER:
			_move_time = 0
			
func is_dead():
	return _fleeing or .is_dead()
	
func damaged_melee(source: Node2D, dir: Vector3, damage: int):
	hurt_timer = 0.5
	vel = dir
	ActorFuncs.set_anim([$Body/Pants, $Body/Shoes], "Normal")
	if not _harmed:
		_harmed = true
		get_parent().civilians_harmed += 1
	if talk_timer <= 0:
		randi()
		var text = ["Ouch!", "Argh!"][randi() % 2]
		var bubble = ActorFuncs.speak(self, text, Vector2(48, 24))
		talk_timer = ceil(bubble.get_time_left() / TALK_ANIM_STEP) * TALK_ANIM_STEP
		bubble.close_after(1)
	
func enter_building(building):
	if  _building_timer <= 0 and randi() % 8 == 0:
		get_tree().call_group("world", "delete_mob", self)
	
func stroll(dir: Vector2):
	_ai_state = AIState.WALK
	_move_speed = rand_range(1.2, 1.5)
	_move_vec = dir.normalized()
	_fleeing = false
	
func flee(dir: Vector2):
	_ai_state = AIState.WALK
	_move_speed = rand_range(2.5, 3.0)
	_move_vec = dir.normalized()
	_fleeing = true
	
func wander():
	_ai_state = AIState.WANDER
	_move_time = 0
	_fleeing = false

func _physics_process(delta):
	if _building_timer > 0:
		_building_timer -= delta
	if hurt_timer > 0:
		if not _fleeing:
			flee(Vector2(vel.x, 0))
		return
	match _ai_state:
		AIState.WALK:
			var move = _move_vec * Vector2(MOVE_SPEED_X, MOVE_SPEED_Y) * _move_speed * delta
			if _move_time > 0:
				_move_time -= delta
				move = _dodge_vec
			else:
				_dodge_vec = Vector2()
				
			vel.x = move.x
			vel.y = move.y
			
			if _move_vec.x < 0:
				scale.x = -1
			elif _move_vec.x > 0:
				scale.x = 1
		AIState.WANDER:
			if _move_time == 0 and randi() % 100 == 0:
				_move_time = randf() * 2 + 1
				_move_vec = Vector2(1, 0).rotated(randi() * PI * 2)
				_move_speed = rand_range(0.7, 0.8)
			
			if _move_time > 0:
				var last = _move_time
				_move_time = max(_move_time - delta, 0)
				var move = _move_vec * Vector2(MOVE_SPEED_X, MOVE_SPEED_Y) * _move_speed * delta * ((last - _move_time) / delta)
				vel.x = move.x
				vel.y = move.y
			
				if _move_vec.x < 0:
					scale.x = -1
				elif _move_vec.x > 0:
					scale.x = 1