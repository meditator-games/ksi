# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends KinematicBody2D

const MOVE_SPEED = Vector2(60, 90)
const CAST_DURATION = 1.0
const HITBOX_DEPTH = 8.0
const FIND_TARGET_TICK = 0.8

var _target_mob_path = null #NodePath
var _target_position = null #Vector2
var _find_target_timer: float = 0
var _cast_timer: float = 0
var _anim_timer: float = 0
var _hurt_timer: float = 0
var _legs_timer: float = 0
var _move_timer: float = 0
var _death_timer: float = 0
var _move_vec: Vector2 = Vector2()
var _move_speed: float = 1.0
var vel: Vector2 = Vector2()
var blink_timer: float = 0

var hp: int = -1
var intellect: int = 1
var aggressiveness: float = 0.0

func _ready():
	$AreaBody.set_meta("body", self)
	set_meta("top", $TopPoint)
	if hp == -1:
		var interval = abs(position.x) / 400.0
		hp = floor(clamp(interval, 3.0, 100.0))
		intellect = floor(clamp(interval * 0.9, 1, 99))
		aggressiveness = floor(clamp(interval * 0.12, 0, 1))


func _process(delta):
	z_index = int(position.y)
		
	if hp <= 0:
		_death_timer += delta
		if _death_timer > 0.5:
			visible = !visible
		if _death_timer > 1.0:
			ActorFuncs.spawn_gold(get_tree(), self.position, clamp(intellect * 0.5, 1, 10))
			get_tree().call_group("world", "delete_mob", self)

func _physics_process(delta):
	if hp > 0:
		var target_mob = get_node(_target_mob_path) if _target_mob_path != null and has_node(_target_mob_path) else null
		
		if _cast_timer > 0:
			_cast_timer -= delta
			$Body/Head/Face.animation = "Normal"
			if $Body/Arms.animation != "Cast":
				if target_mob == null:
					_ice_attack(Vector2(-scale.x, 0))
				else:
					_ice_attack((target_mob.position - position).normalized())
				$Body/Arms.animation = "Cast"
			$Body/Head.scale.x = 1
			$Body.position.y = 0
			if randf() < 0.1 - aggressiveness * 0.1:
				_target_mob_path = null
		elif _move_timer > 0:
			$Body/Head.scale.x = 1
			$Body.position.y = 0
			_walk(delta)
		elif _hurt_timer <= 0:
			$Body/Arms.animation = "Normal"
			# try to find a target
			if target_mob == null:
				_find_target_timer -= delta
				if _find_target_timer <= 0:
					if randf() < aggressiveness:
						target_mob = ActorFuncs.find_target(get_parent(), aggressiveness)
						if target_mob != null:
							_target_mob_path = target_mob.get_path()
					else:
						$Body/Head.scale.x *= -1
						_find_target_timer = FIND_TARGET_TICK
						if randi() % 6 == 0:
							_start_wander()
			
			if target_mob == null:
				_anim_timer = fmod(_anim_timer + delta, 0.9)
				$Body.position.y = floor((_anim_timer / 0.9) * 2.0)
			else:
				$Body/Head.scale.x = 1
				$Body.position.y = 0
				_chase(target_mob, delta)
				
	# XY movement
	var collision: KinematicCollision2D = move_and_collide(Vector2(vel.x, vel.y), true, true, true)
	if collision != null:
		vel = collision.travel
		_move_timer = 0
		_target_position = null
	position += vel
	
	if hp > 0 and _hurt_timer <= 0 and _cast_timer <= 0:
		if abs(vel.x) > 0.1 or abs(vel.y) > 0.1:
			_legs_timer = fmod(_legs_timer + delta * vel.length() * 2.0, 1.0)
			$Legs.animation = ["Walk1", "Normal", "Walk2", "Normal"][floor(_legs_timer * 4)]
		else:
			$Legs.animation = "Normal"
			_legs_timer = 0
		ActorFuncs.blink(self, $Body/Head/Face, delta)
		if $Body/Head/Face.animation == "Normal":
			$Body/Head/Face.animation = "Grin"
	
	if _hurt_timer > 0:
		_hurt_timer -= delta
	
	vel.x *= 1 - delta * 6    # friction
	vel.y *= 1 - delta * 6

func _walk(delta):
	if _move_timer == 0 and randi() % 100 == 0:
		_start_wander()
	
	if _move_timer > 0:
		var last = _move_timer
		_move_timer = max(_move_timer - delta, 0)
		var move = _move_vec * MOVE_SPEED * _move_speed * delta * ((last - _move_timer) / delta)
		vel.x = move.x
		vel.y = move.y
	
		if _move_vec.x < 0:
			scale.x = 1
		elif _move_vec.x > 0:
			scale.x = -1

func _start_wander():
	_move_timer = randf() * 2 + 1
	_move_vec = Vector2(1, 0).rotated(randi() * PI * 2)
	_move_speed = rand_range(0.7, 0.8)

func _chase(target_mob, delta):
	_cast_timer = CAST_DURATION * (1.0 - aggressiveness * 0.4)

func _ice_attack(direction: Vector2):
	var spell = preload("res://actors/IceSpell.tscn").instance()
	spell.position = position
	spell.direction = direction
	spell.damage = intellect
	spell.max_distance = 200.0 * (aggressiveness + 1.0)
	spell.set_meta("owner_path", get_path())
	get_tree().call_group("world", "add_mob", spell)

func is_dead():
	return hp <= 0
	
func damaged_melee(source: Node2D, dir: Vector3, damage: int):
	if _hurt_timer > 0 or hp <= 0:
		return
	hp -= damage
	_move_timer = 0
	_hurt_timer = 0.5
	_cast_timer = 0
	_anim_timer = 0
	_legs_timer = 0
	vel = Vector2(dir.x, dir.y)
	$Legs.animation = "Walk1"
	$Body/Arms.animation = "Normal"
	if hp > 0:
		$Body/Head/Face.animation = "Hurt"
		if source.has_meta("owner_path"):
			_target_mob_path = source.get_meta("owner_path")
		else:
			_target_mob_path = source.get_path()
		aggressiveness += 0.1
	else:
		$Body/Head/Face.animation = "Dead"