# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends KinematicBody2D

var hp = randi() % 10
var PARTICLE_THRUST = 5.0
var HITBOX_DEPTH = 8.0

func _ready():
	$AreaBody.set_meta("body", self)
	if randi() % 2 == 0:
		$Hat.visible = false
		$HatDetail.visible = false

func _process(delta):
	z_index = int(position.y)
	
func is_dead():
	return false
	
func damaged_melee(source: Node2D, dir: Vector3, damage: int):
	hp -= damage
	if hp <= 0:
		var particles = preload("res://actors/DeathParticles.tscn").instance()
		particles.position = position + Vector2(0, -28)
		particles.process_material.set_shader_param("initial_thrust", dir * (1 - hp) * PARTICLE_THRUST)
		ActorFuncs.oneshot_particles(particles, get_parent())
		get_tree().call_group("world", "delete_mob", self)
		ActorFuncs.spawn_gold(get_tree(), self.position, randi() % 3 + 1)
		