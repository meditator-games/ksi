# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends KinematicBody2D

const SPEED = Vector2(80.0, 120.0)
const SPAWN_INTERVAL = 15.0

var max_distance: float = 100.0
var direction: Vector2 = Vector2()
var damage: int = 1
var _distance_accumulator = -SPAWN_INTERVAL/2.0
var _total_distance = 0.0

func _physics_process(delta):
	if direction.x == 0 and direction.y == 0:
		_destroy()
		return
	var vel = direction * SPEED * delta
	var collision: KinematicCollision2D = move_and_collide(vel, true, true, true)
	if collision != null:
		if _distance_accumulator > 0:
			_destroy()
		vel = collision.travel
		
	_distance_accumulator += vel.length()
	while _distance_accumulator > SPAWN_INTERVAL:
		_spawn_icicle(position + vel * (_distance_accumulator - SPAWN_INTERVAL))
		_distance_accumulator -= SPAWN_INTERVAL
		_total_distance += SPAWN_INTERVAL
		if _total_distance >= max_distance:
			_destroy()
	position += vel
	
func _destroy():
	get_tree().call_group("world", "delete_mob", self)

func _spawn_icicle(pos: Vector2):
	var icicle = preload("res://actors/Icicle.tscn").instance()
	icicle.set_meta("owner_path", get_meta("owner_path"))
	icicle.position = pos
	icicle.damage = damage
	get_tree().call_group("world", "add_mob", icicle)