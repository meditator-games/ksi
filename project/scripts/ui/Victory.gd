# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Node

var stats: Dictionary = {}
var _return_timer = 3.0

func _ready():
	get_tree().connect("screen_resized", Global, "center_viewport", [$Display])
	var total = 0
	if stats.has("stat_score"):
		$Center/Margins/Box/Stats/Score.text = str(stats["stat_score"])
		total += stats["stat_score"]
	if stats.has("max_distance"):
		$Center/Margins/Box/Stats/Dist.text = str(stats["max_distance"])
		total += stats["max_distance"]
	if stats.has("civilians_harmed"):
		$Center/Margins/Box/Stats/Civ.text = "-" + str(stats["civilians_harmed"])
		total -= stats["civilians_harmed"]
	if stats.has("total_gp"):
		$Center/Margins/Box/Stats/Money.text = str(stats["total_gp"] / 10)
		total += stats["total_gp"] / 10
		
	$Center/Margins/Box/Stats/Final.text = str(total)
	
func _process(delta):
	if _return_timer > 0:
		_return_timer -= delta
		if _return_timer <= 0:
			$Center/Margins/Box/ReturnButton.disabled = false
			$Center/Margins/Box/ReturnButton.grab_focus()

func _on_ReturnButton_pressed():
	$Confirm.play()
	Global.fade_to(Global, "quit_to_title")
