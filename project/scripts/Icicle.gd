# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends AnimatedSprite

const THRUST = 2.0

onready var _shape_extents = $Area/Shape.shape.extents
onready var _shape_position = $Area/Shape.position

var damage: int = 1

func _ready():
	$Area/Shape.shape.extents = Vector2()
	$Area/Shape.position = Vector2()
	play()
	
func _on_Area_entered(area):
	var body = ActorFuncs.get_attack_body(self, area)
	if body != null:
		var diff = self.position - body.position
		body.damaged_melee(self, Vector3(diff.x, diff.y, 32.0).normalized() * -THRUST, damage)

func _process(delta):
	z_index = int(position.y)
	
func _physics_process(delta):
	var step = 0
	if frame < 5:
		step = frame / 5.0
	elif frame < 9:
		step = 1
	else:
		step = clamp(1 - (frame - 9) / 8.0, 0, 1)
	$Area/Shape.shape.extents = Vector2().linear_interpolate(_shape_extents, step)
	$Area/Shape.position = Vector2().linear_interpolate(_shape_position, step)
	for area in $Area.get_overlapping_areas():
		_on_Area_entered(area)

func _on_animation_finished():
	get_tree().call_group("world", "delete_mob", self)
