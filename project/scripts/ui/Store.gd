# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Control

const CHAR_SPEED = 0.02

onready var ItemGrid = $Center/StorePanel/Margins/VBox/HBox/Items
onready var BuyButton = $Center/StorePanel/Margins/VBox/HBox/Items/BuyButton
onready var CostLabel = $Center/StorePanel/Margins/VBox/HBox/Items/CostLabel
onready var DialogueBox = $Center/StorePanel/Margins/VBox/PanelContainer/Dialogue
onready var GP = $Center/StorePanel/Margins/VBox/HBoxContainer/GP
var _text_subcounter: float = 0
var _item_buttons = []
var _leaving: bool = false
var items = []
var title = "Store"
var welcome_message = "Hello"
var leave_message = "Leaving"
var purchase_message = "Thanks"
var broke_message = "No"
var exit_message = "Bye"
var player = null
var clerk = null
var say_count = 3


func _ready():
	if player != null:
		GP.text = str(player.gp)
	if randi() % 2 == 0:
		clerk = $Center/StorePanel/Margins/VBox/HBox/DummyF
		$Center/StorePanel/Margins/VBox/HBox/DummyM.queue_free()
	else:
		clerk = $Center/StorePanel/Margins/VBox/HBox/DummyM
		$Center/StorePanel/Margins/VBox/HBox/DummyF.queue_free()
	clerk.random_colors()
	clerk.visible = true
	$BG.modulate = Color.from_hsv(randf(), 0.93, 0.48)
	$Center/StorePanel/Margins/VBox/Title.text = title
	var index = 0
	for item in items:
		var button = BuyButton.duplicate()
		var cost = CostLabel.duplicate()
		button.text = item.name
		cost.text = str(item.cost)
		button.connect("focus_entered", self, "say_and_blip", [item.desc])
		button.connect("mouse_entered", self, "say", [item.desc])
		button.connect("pressed", self, "_buy_item", [index])
		_item_buttons.append(button)
		ItemGrid.add_child(button)
		ItemGrid.add_child(cost)
		if index == 0:
			button.grab_focus()
		index += 1
	BuyButton.free()
	CostLabel.free()
	$Center/StorePanel/Margins/VBox/LeaveButton.connect("focus_entered", self, "say_and_blip", [leave_message])
	$Center/StorePanel/Margins/VBox/LeaveButton.connect("mouse_entered", self, "say", [leave_message])
	var timer = Timer.new()
	add_child(timer)
	timer.start(0.2)
	yield(timer, "timeout")
	say(welcome_message)
	
func say_and_blip(text):
	if say_count == 0:
		$Blip.play()
	say(text)

func _buy_item(index: int):
	var item = items[index]
	if player.spend(item.cost):
		say(purchase_message)
		player.give(item.values)
		GP.text = str(player.gp)
	else:
		say(broke_message)

func _process(delta):
	if DialogueBox.visible_characters < DialogueBox.text.length():
		_text_subcounter += delta
		while _text_subcounter > CHAR_SPEED and DialogueBox.visible_characters < DialogueBox.text.length():
			DialogueBox.visible_characters += 1
			_text_subcounter -= CHAR_SPEED
		$Talk.playing = say_count == 0
	else:
		$Talk.playing = false
		
	if Input.is_action_just_pressed("ui_cancel"):
		leave()

func say(text):
	if say_count > 0:
		say_count -= 1
	if _leaving:
		return
	if typeof(text) == TYPE_ARRAY:
		DialogueBox.text = str(text[randi() % text.size()])
	else:
		DialogueBox.text = str(text)
	DialogueBox.visible_characters = 0
	clerk.talk_timer = DialogueBox.text.length() * CHAR_SPEED

func _destroy():
	var door = $Door
	remove_child(door)
	get_tree().root.add_child(door)
	door.play()
	get_tree().paused = false
	player.frozen = false
	visible = false
	player.get_parent().visible = true
	player.get_parent().fade_in_music()
	Global.remove_overlay(self)

func leave():
	for b in _item_buttons:
		b.disabled = true
		b.focus_mode = FOCUS_NONE
	$Center/StorePanel/Margins/VBox/LeaveButton.disabled = true
	say(exit_message)
	_leaving = true
	var timer = Timer.new()
	add_child(timer)
	timer.start(1.0)
	$Leave.play()
	yield(timer, "timeout")
	Global.fade_out_music($Music)
	Global.fade_to(self, "_destroy")
