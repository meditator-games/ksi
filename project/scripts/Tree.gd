# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

tool
extends Node2D

export (int) var height setget set_height, get_height

func get_rect():
	var rect = $Top.get_rect()
	rect.size.y = 128 + height
	return rect

func _ready():
	z_index = position.y + 128 + height
	$TrunkContainer/Trunk.position.y = height

func get_height():
	return height

func set_height(h: int):
	height = h