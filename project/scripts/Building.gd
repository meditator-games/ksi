# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends StaticBody2D

var type = ""

func get_rect():
	return $Structure.get_rect()

func get_sign_text():
	return $Text/Viewport/Label.text 

func set_sign_text(text: String):
	$Text/Viewport/Label.text = text
	$Text/Viewport.render_target_update_mode = $Text/Viewport.UPDATE_ONCE

func random_colors(b_seed):
	var random = RandomNumberGenerator.new()
	random.seed = b_seed
	random.randf()
	var hue = random.randf()
	var saturation = 0.0 if random.randi() % 2 == 0 else -0.2
	$Structure.material.set_shader_param("hue_adjust", hue)
	$Structure.material.set_shader_param("saturation_adjust", saturation)

func _on_Entrance_body_entered(body):
	if body.has_method("enter_building"):
		body.enter_building(self)
