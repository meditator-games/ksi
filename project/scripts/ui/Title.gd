# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Node

onready var _panel_stack = [$VPContainer/Viewport/Title]
onready var NewContainer = $VPContainer/Viewport/NewContainer

func _refocus(old = null):
	var current = _panel_stack[-1]
	if current == $VPContainer/Viewport/Title:
		if old == $VPContainer/Viewport/SettingsDialog:
			$VPContainer/Viewport/Title/Buttons/VBox/SettingsButton.grab_focus()
		elif old == $VPContainer/Viewport/Credits:
			$VPContainer/Viewport/Title/Buttons/VBox/CreditsButton.grab_focus()
		else:
			$VPContainer/Viewport/Title/Buttons/VBox/NewButton.grab_focus()
	elif current == $VPContainer/Viewport/NewContainer:
		$VPContainer/Viewport/NewContainer/NewDialog/Margins/ParamVBox/GenerateButton.grab_focus()
	elif current == $VPContainer/Viewport/SettingsDialog:
		current.refocus()
	elif current == $VPContainer/Viewport/Credits:
		current.refocus()

func _ready():
	randomize()
	get_tree().connect("screen_resized", self, "_resized")
	_resized()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	_panel_stack[0].visible = true
	_refocus()
	Settings.update_current_viewport()

func _resized():
	Global.center_viewport($Display)
	Global.center_viewport($VPContainer)

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
#		if _loading != null:
#			_loading.free()
#			_loading = null
#			_set_new_game_loading(false)
		if _panel_stack.size() > 1:
			_pop_menu()

func _fade(from: Control, to: Control):
	$Fader.interpolate_property($VPContainer, "modulate", null, Color(1.0, 1.0, 1.0, 0.0), 0.4, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Fader.start()
	yield($Fader, "tween_completed")
	$Fader.remove_all()
	from.visible = false
	to.visible = true
	$Fader.interpolate_property($VPContainer, "modulate", null, Color(1.0, 1.0, 1.0, 1.0), 0.4, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Fader.start()
	_refocus(from)
	yield($Fader, "tween_completed")
	$Fader.remove_all()
	
func _push_menu(panel: Control):
	if not $Fader.is_active():
		_panel_stack.push_back(panel)
		_fade(_panel_stack[-2], _panel_stack[-1])

func _pop_menu():
	if not $Fader.is_active():
		var last = _panel_stack.pop_back()	
		_fade(last, _panel_stack[-1])

func _on_NewButton_pressed():
	$Confirm.play()
	NewContainer.randomize_seed()
	NewContainer.randomize_dummy()
	_push_menu(NewContainer)

func _on_SettingsButton_pressed():
	$Confirm.play()
	_push_menu($VPContainer/Viewport/SettingsDialog)

func _on_CreditsButton_pressed():
	$Confirm.play()
	_push_menu($VPContainer/Viewport/Credits)

func _on_QuitButton_pressed():
	$Confirm.play()
	Global.fade_to(Global, "quit")

func _on_Settings_BackButton_pressed():
	_pop_menu()

func _on_Credits_BackButton_pressed():
	_pop_menu()
	
func _on_NewGame_BackButton_pressed():
	_pop_menu()

func _on_GenerateButton_pressed():
	$Confirm.play()
	Global.fade_to(Global, "new_game", [NewContainer.get_seed(), NewContainer.get_values()])

