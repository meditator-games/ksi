# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Node

const DEFAULT_FADE_DURATION = 0.7

var fade_cb = null
var fade_overlay = null
var last_value = 1
var current_scene = null
var fade_duration = DEFAULT_FADE_DURATION
var fade_color: Color = Color(1.0, 1.0, 1.0, 1.0)
var overlay_stack = []
var fade_value = 0

func _ready():
	_start_fade(false)

func _enter_tree():
	var root = get_tree().root
	current_scene = root.get_child(root.get_child_count() -1)
	get_tree().current_scene = current_scene
	_recreate_fade_overlay()

func _start_fade(on):
	var target = 0
	if on:
		target = 1
	$Tween.remove_all()
	$Tween.interpolate_property(self, "fade_value", null, target, fade_duration, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()

func _process(delta):
	last_value = fade_value
	if not $Tween.is_active():
		if fade_cb != null:
			fade_cb[0].callv(fade_cb[1], fade_cb[2])
			fade_cb = null
		elif fade_value > 0:
			_start_fade(false)
	fade_overlay.visible = fade_value > 0
	if fade_overlay.visible:
		fade_overlay.material.set_shader_param("opacity", fade_value)

func _recreate_fade_overlay():
	fade_overlay = preload("res://ui/FadeOverlay.tscn").instance()
	fade_overlay.material.set_shader_param("opacity", last_value)
	fade_overlay.name = "FadeOverlay"
	fade_overlay.modulate = fade_color
	get_tree().root.get_children().back().add_child(fade_overlay)
	
func goto_scene(scene: Node):
	current_scene.free()
	current_scene = scene
	get_tree().root.add_child(current_scene)
	get_tree().current_scene = current_scene
	_recreate_fade_overlay()

func set_fade_color(color: Color):
	fade_color = color
	fade_overlay.modulate = color

func fade_to(obj, method, args=[]):
	fade_cb = [obj, method, args]
	fade_overlay.visible = true
	_start_fade(true)
	
func push_overlay(control, modal=false):
	if not overlay_stack.empty():
		overlay_stack[-1]["control"].visible = false
	overlay_stack.append({"control": control, "modal": modal})
	current_scene.add_child(control)
	if current_scene.has_node("HUD"):
		current_scene.move_child(control, current_scene.get_node("HUD").get_index())
	elif fade_overlay != null and fade_overlay.get_parent() == current_scene:
		current_scene.move_child(control, fade_overlay.get_index())

func clear_overlays():
	for entry in overlay_stack:
		entry["control"].queue_free()
	overlay_stack = []
	
func remove_overlay(control):
	for entry in overlay_stack:
		if entry["control"] == control:
			overlay_stack.erase(entry)
			control.queue_free()
			break
	if not overlay_stack.empty():
		overlay_stack[-1]["control"].visible = true

func is_overlay_modal():
	if not overlay_stack.empty():
		return overlay_stack[-1]["modal"]
	return false

func current_overlay():
	if not overlay_stack.empty():
		return overlay_stack[-1]["control"]
	return null

var cheat = PoolByteArray()
func _input(event):
	if event is InputEventKey and event.pressed and event.scancode >= KEY_A and event.scancode <= KEY_Z:
		cheat.append(event.scancode + 1)
		if cheat.size() > 16:
			cheat.remove(0)
		get_tree().call_group("world", "try_cheat", cheat.get_string_from_ascii())
		
func format_action_name(action: String):
	var words = action.split("_")
	for i in range(len(words)):
		words[i] = words[i][0].to_upper() + words[i].substr(1, words[i].length() - 1)
	return words.join(" ")

func event_to_text(event: InputEvent):
	match event.get_class():
		"InputEventJoypadMotion":
			return "Pad " + ["X","Y"][event.axis % 2] + str(event.axis / 2) + ("-" if event.axis_value == -1 else "+")
		"InputEventJoypadButton":
			return "Pad B" + str(event.button_index)
		_:
			return event.as_text()
			
func get_bind_str(action):
	var keys = PoolStringArray()
	for key in InputMap.get_action_list(action):
		keys.push_back(event_to_text(key))
	return keys.join(" or ")

func new_game(world_seed: int, player_params: Dictionary):
	fade_duration = DEFAULT_FADE_DURATION
	get_tree().paused = false
	var game = preload("res://scenes/Game.tscn").instance()
	game.setup(world_seed, player_params)
	goto_scene(game)

func quit_to_title():
	fade_duration = DEFAULT_FADE_DURATION
	get_tree().paused = false
	goto_scene(preload("res://ui/Title.tscn").instance())
	
func victory(stats):
	clear_overlays()
	fade_duration = DEFAULT_FADE_DURATION
	get_tree().paused = false
	var victory = preload("res://ui/Victory.tscn").instance()
	victory.stats = stats
	goto_scene(victory)
	
func quit():
	get_tree().quit()
	
func get_game_viewport():
	if current_scene != null and current_scene.has_node("Display"):
		return current_scene.get_node("Display")
	return null

func center_viewport(control: Control):
	var vp: Viewport
	if control.has_node("Viewport"):
		vp = control.get_node("Viewport")
	else:
		vp = control.get_parent().get_node(control.texture.viewport_path)
	var screen_ratio = vp.size.x / vp.size.y
	var ws = OS.window_size
	var ratio = ws.x / ws.y
	if ratio > screen_ratio:
		control.rect_position = Vector2((vp.size.x * (ws.y / vp.size.y) - ws.x) / -2, 0)
	else:
		control.rect_position = Vector2(0, (vp.size.y * (ws.x / vp.size.x) - ws.y) / -2)

func fade_out_music(music: AudioStreamPlayer):
	$MusicFader.stop_all()
	$MusicFader.remove_all()
	$MusicFader.interpolate_property(music, "volume_db", null, -80, fade_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$MusicFader.start()

func fade_in_music(music: AudioStreamPlayer):
	music.stream_paused = false
	$MusicFader.stop_all()
	$MusicFader.remove_all()
	$MusicFader.interpolate_property(music, "volume_db", null, 0, fade_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$MusicFader.start()

func _on_Tween_tween_completed(object, key):
	$Tween.remove(object, key)
