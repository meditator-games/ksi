# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends KinematicBody2D

const PUNCH_THRUST = 2.0
const HITBOX_DEPTH = 8.0
const TALK_ANIM_STEP = 0.2

enum DummyState {
	STAND, WALK, JUMP, FALL, DEAD
}

signal body_collided(obj)

var state: int = DummyState.STAND
var legs_timer: float = 0
var hurt_timer: float = 0
var talk_timer: float = 0
var punch_timer: float = 0
var punch_init: bool = true
var blink_timer: float = randf() * 3.0 + 2.0
var vel: Vector3 = Vector3()
var thrust_vel: Vector3 = Vector3()
export (bool) var disable_z_index = false

func _ready():
	$Body/AreaBody.set_meta("body", self)
	set_meta("top", $Body/TopPoint)

func _process(delta):
	if not disable_z_index:
		z_index = int(position.y)
	
func _physics_process(delta):
	# XY movement
	var collision: KinematicCollision2D = move_and_collide(Vector2(vel.x + thrust_vel.x, vel.y + thrust_vel.y), true, true, true)
	if collision != null:
		thrust_vel.x = 0
		thrust_vel.y = 0
		vel.x = collision.travel.x
		vel.y = collision.travel.y
	position += Vector2(vel.x + thrust_vel.x, vel.y + thrust_vel.y)
	if collision != null:
		emit_signal("body_collided", collision)
	
	# Z movement
	collision = $Body.move_and_collide(Vector2(0, vel.z + thrust_vel.z), true, true, true)
	if collision != null:
		thrust_vel.z = 0
		vel.z = collision.travel.y
	$Body.position.y += vel.z + thrust_vel.z
	if collision != null:
		emit_signal("body_collided", collision)
	
	if state != DummyState.DEAD:
		if vel.z < -0.001:
			state = DummyState.JUMP
		elif vel.z > 0.001 and $Body.position.y < 0:
			state = DummyState.FALL
		elif abs(vel.x) > 0.001 or abs(vel.y) > 0.001:
			if state != DummyState.WALK:
				legs_timer = 0
				state = DummyState.WALK
		else:
			legs_timer = 0
			state = DummyState.STAND
		
	match state:
		DummyState.WALK:
			legs_timer = fmod(legs_timer + delta * vel.length() * 3.0, 1.0)
			ActorFuncs.set_anim([$Body/Pants, $Body/Shoes], ["Walk1", "Normal", "Walk2", "Normal"][floor(legs_timer * 4)])
		DummyState.STAND:
			ActorFuncs.set_anim([$Body/Pants, $Body/Shoes], "Normal")
		DummyState.JUMP:
			ActorFuncs.set_anim([$Body/Pants, $Body/Shoes], "Walk1")
		DummyState.FALL:
			ActorFuncs.set_anim([$Body/Pants, $Body/Shoes], "Walk1")
		DummyState.DEAD:
			$Shadow.modulate.a = 0
			punch_timer = 0
			ActorFuncs.set_anim([$Body/Head, $Body/Eyes, $Body/Pupils], "Hurt")
			ActorFuncs.set_anim([$Body/Pants, $Body/Shoes], "Normal")
	
	if punch_timer > 0:
		ActorFuncs.set_anim([$Body/Shirt, $Body/Hands], "Punch")
		punch_timer -= delta
		if punch_init: # only monitor for hits on the first frame
			$Body/AreaFist.monitoring = true
			punch_init = false
		else:
			$Body/AreaFist.monitoring = false
	else:
		ActorFuncs.set_anim([$Body/Shirt, $Body/Hands], "Normal")
		$Body/AreaFist.monitoring = false
	
	thrust_vel.x *= 1 - delta * 6    # friction
	thrust_vel.y *= 1 - delta * 6
	vel.x *= 1 - delta * 6 
	vel.y *= 1 - delta * 6
	if $Body.position.y >= 0 and vel.z >= 0:
		vel.z = 0
		thrust_vel.z = 0
		$Body.position.y = 0
	else:
		vel.z += 9.8 * delta
	
	if state != DummyState.DEAD:
		$Shadow.modulate.a = pow(clamp(-$Body.position.y / 128.0, 0.25, 1), 1.0/2.0)
		if hurt_timer > 0:
			hurt_timer -= delta
			ActorFuncs.set_anim([$Body/Head, $Body/Eyes, $Body/Pupils], "Hurt")
			if talk_timer > 0:
				talk_timer -= delta
		elif talk_timer > 0:
			talk_timer -= delta
			ActorFuncs.set_anim([$Body/Eyes, $Body/Pupils], "Normal")
			$Body/Head.animation = ["Talk", "Normal"][floor(fmod(talk_timer / TALK_ANIM_STEP, 2))]
		else:
			ActorFuncs.set_anim([$Body/Eyes, $Body/Pupils], "Normal")
			ActorFuncs.blink(self, $Body/Head, delta)

func is_dead():
	return state == DummyState.DEAD

func get_colors():
	return {
		"hat": $Body/Hat.modulate,
		"hair": $Body/Hair.modulate,
		"eyes": $Body/Pupils.modulate,
		"skin": $Body/Head.modulate,
		"shirt": $Body/Shirt.modulate,
		"pants":$Body/Pants.modulate,
		"shoes": $Body/Shoes.modulate
	}

func set_colors(colors):
	if colors.has("hat"):
		$Body/Hat.modulate = colors["hat"]
	if colors.has("hair"):
		$Body/Hair.modulate = colors["hair"]
	if colors.has("eyes"):
		$Body/Pupils.modulate = colors["eyes"]
	if colors.has("skin"):
		$Body/Head.modulate = colors["skin"]
		$Body/Hands.modulate = colors["skin"]
	if colors.has("shirt"):
		$Body/Shirt.modulate = colors["shirt"]
	if colors.has("pants"):
		$Body/Pants.modulate = colors["pants"]
	if colors.has("shoes"):
		$Body/Shoes.modulate = colors["shoes"]
	
func _random_from_generator(gen):
	$Body/Hat.modulate =  Color.from_hsv([0.0, 1.0/6.0, 2.0/6.0, 3.0/6.0, 4.0/6.0, 0.75][gen.randi() % 6], 1.0, 1.0)
	$Body/Head.modulate = Color("524542").linear_interpolate(Color("ffffff"), gen.randf())
	$Body/Hands.modulate = $Body/Head.modulate
	$Body/Hair.modulate = Color("f6f5c0").linear_interpolate(Color("df9177"), gen.randf()).linear_interpolate(Color("1e1d1b"), gen.randf())
	$Body/Shirt.modulate = Color.from_hsv(gen.randf(), 0.5, 0.9)
	$Body/Pants.modulate =  Color.from_hsv(gen.randf(), 0.5, 0.6)
	$Body/Pupils.modulate =  Color.from_hsv([0.08, 0.45, 0.55, 0.65][gen.randi() % 4], gen.randf() * 0.3 + 0.6, gen.randf() * 0.5 + 0.25)

func random_colors_from_seed(seed_value: int):
	var random = RandomNumberGenerator.new()
	random.seed = seed_value
	_random_from_generator(random)
	
class GlobalRandomGenerator:
	static func randi():
		return randi()
	static func randf():
		return randf()
		
func random_colors():
	_random_from_generator(GlobalRandomGenerator)