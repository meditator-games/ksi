# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Node2D

const THRUST = 1.5
const SPEED = 130.0

var direction: Vector2 = Vector2()
var speed = 1.0
var damage = 1

func set_z(z):
	$Height.position.y = -z

func _on_Area_entered(area):
	if ActorFuncs.melee_attack(self, area, THRUST * speed, damage) != null:
		var particles = preload("res://actors/SnowHitParticle.tscn").instance()
		particles.position = position + $Height.position
		particles.process_material.set_shader_param("initial_thrust", direction * speed * THRUST * 30.0)
		ActorFuncs.oneshot_particles(particles, get_parent())
		get_tree().call_group("world", "delete_mob", self)

func _process(delta):
	z_index = int(position.y)
	
func _physics_process(delta):
	position += direction * SPEED * speed * delta
	for area in $Height/Area.get_overlapping_areas():
		_on_Area_entered(area)