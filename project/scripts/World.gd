# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Node2D

const SCENERY_STEP = 32
const MAX_SCENERY_WIDTH = SCENERY_STEP * 32
const CHUNK_STEP = SCENERY_STEP * 128
const VIEWPORT_WIDTH = 400
const OBJ_WIDTHS = {
	"building-small": SCENERY_STEP * 8,
	"building-tall": SCENERY_STEP * 8,
	"building-wide": SCENERY_STEP * 18,
	"tree": SCENERY_STEP * 2
}
const WALKER_SPAWN_INTERVAL = 4.0

export (NodePath) var music_path = null

var _scroll_pairs: Array = []
var _chunks: Dictionary = {}
var _active_scenery: Dictionary = {}
var _cant_respawn = []  # x spots that are blocked from spawning mobs until they are far outside the camera
var _walker_spawn_timer = WALKER_SPAWN_INTERVAL
var world_seed: float = 0
var player: KinematicBody2D = null
var mobs = []

var max_distance: int = 0
var civilians_harmed: int = 0
var total_gp: int = 0

onready var random = RandomNumberGenerator.new()
onready var simplex = OpenSimplexNoise.new()

func get_stats() -> Dictionary:
	return {
		"stat_score": player.max_hp + player.strength + player.dexterity + player.constitution,
		"max_distance": max_distance,
		"civilians_harmed": civilians_harmed,
		"total_gp": total_gp
	}

class Powerup:
	var cost
	var name
	var desc
	var values
	func _init(cost: int, name: String, desc: String, values: Dictionary):
		self.cost = cost
		self.name = name
		self.desc = desc
		self.values = values
		
static func whitenoise(x, y=0, z=0, w=0):
    return fmod((sin(12.9898 * x + 78.233 * y + 17.3392 * z + 47.743 * w) * 43758.5453), 1.0)

func fade_out_music():
	if music_path != null and has_node(music_path):
		Global.fade_out_music(get_node(music_path))
func fade_in_music():
	if music_path != null and has_node(music_path):
		Global.fade_in_music(get_node(music_path))

const Store = preload("res://ui/Store.tscn")

func enter_building(building):
	var name = building.get_sign_text()
	var store
	match building.type:
		"food":
			store = _enter_restaurant(building)
		"army":
			store = _enter_army_surplus(building)
		"super":
			store = _enter_superstore(building)
		_:
			return
	visible = false
	get_tree().paused = true
	Global.push_overlay(store, true)
	if music_path != null and has_node(music_path):
		get_node(music_path).stream_paused = true
		
func _create_store(building):
	var store = Store.instance()
	store.title = building.get_sign_text()
	store.player = player
	return store
	
func _enter_restaurant(building):
	var store = _create_store(building)
	store.items.append(Powerup.new(10, "Coffee", "Fresh roasted!\nCures 2HP.", {"hp": 2}))
	store.items.append(Powerup.new(60, "Sandwich", "Only the finest\ningredients!\nCures 15HP.", {"hp": 15}))
	if int(abs(building.position.x)) % 3 == 0:
		store.items.append(Powerup.new(70, "Fish Fillet", "A rare delicacy!\nCures 20HP.", {"hp": 15}))
	if int(abs(building.position.x)) % 3 == 1:
		store.items.append(Powerup.new(250, "Magic Spice", "From a far off\nmystical land.\nIncreases CON by 1.", {"constitution": 1}))
	if int(abs(building.position.x)) > 1200:
		store.items.append(Powerup.new(300, "Secret Sauce", "It's our special\nsecret.\nIncreases max HP by 2.", {"max_hp": 2}))
	if int(abs(building.position.x)) > 8000 and int(abs(building.position.x)) % 6 == 0:
		store.items.append(Powerup.new(5000, "Win the Game", "Buy this and you win!", {"win": true}))
	#if int(abs(building.position.x)) % 6 == 0:
		#store.items.append(Powerup.new(500, "Frozen Swordfish", "An unusual catch!\nEQUIP: Increases melee\nrange.", {"weapons":["swordfish"]}))
	store.welcome_message = "Fancy a bite to eat?"
	store.purchase_message = ["Would you like anything\nelse?", "You're my best customer!"]
	store.leave_message = "Leaving so soon?"
	store.exit_message = "See ya later."
	store.broke_message = "You don't have enough!"
	return store
	
func _enter_army_surplus(building):
	var store = _create_store(building)
	store.items.append(Powerup.new(100, "Food Ration", "Just add water.\nCures 10HP.", {"hp": 10}))
	if int(abs(building.position.x)) > 1800:
		store.items.append(Powerup.new(250, "Protein Ration", "It makes you feel a lot\nbetter than it tastes.\nIncreases STR by 1.", {"strength": 1}))
		if int(abs(building.position.x)) % 3 == 0:
			store.items.append(Powerup.new(300, "Energy Drink", "Tense those nerves.\nIncreases DEX by 1.", {"dexterity": 1}))
	if int(abs(building.position.x)) > 8000 and int(abs(building.position.x)) % 6 == 0:
		store.items.append(Powerup.new(5000, "Win the Game", "Buy this and you win!", {"win": true}))
	#store.items.append(Powerup.new(2000, "Snowball Cannon", "Take 'em out from a\ndistance. EQUIP: Shoots\nprojectiles.", {"weapons": ["bazooka"]}))
	store.welcome_message = "At ease, recruit!"
	store.purchase_message = ["Glad to be of use.", "That'll come in handy."]
	store.leave_message = "Back to the front lines."
	store.exit_message = "You're always welcome\nin our troop."
	store.broke_message = "Sorry but I can't give\nit to ya for free."
	return store
	
func _enter_superstore(building):
	var store = _create_store(building)
	store.items.append(Powerup.new(20, "Fries", "Comes with free ketchup.\nCures 5HP.", {"hp": 5}))
	store.items.append(Powerup.new(60, "Burger", "Do you want pickles on\nthat?\nCures 15HP.", {"hp": 15}))
	store.items.append(Powerup.new(200, "Deluxe Combo Meal", "Cures 50HP.\nDecreases STR by 1.", {"hp": 50, "strength":-1}))
	if int(abs(building.position.x)) > 1800:
		store.items.append(Powerup.new(450, "Mega Shake", "Contains all your daily\nvitamins and minerals.\nIncreases max HP by 3.", {"max_hp": 3}))
	if int(abs(building.position.x)) > 2400:
		store.items.append(Powerup.new(300, "Adreno-Cola", "Liquid electricity.\nIncreases DEX by 1.", {"dexterity": 1}))
		store.items.append(Powerup.new(2700, "Ultra Shake", "Increases max HP, STR,\nDEX, CON by 1.", {"max_hp": 1, "strength": 1, "dexterity": 1, "constitution": 1}))
		if int(abs(building.position.x)) % 3 == 1:
			store.items.append(Powerup.new(220, "Exotic Cheese", "It smells funny.\nIncreases CON by 1.", {"constitution": 1}))
		store.items.append(Powerup.new(250, "Barbell Set", "Feel the pump.\nIncreases STR by 1.", {"strength": 1}))
		#store.items.append(Powerup.new(6000, "Ice Machine", "Seems to be functioning\noddly. EQUIP: Casts icicle magic.", {"weapons": ["icicle"]}))
	if int(abs(building.position.x)) > 8000 and int(abs(building.position.x)) % 6 == 0:
		store.items.append(Powerup.new(5000, "Win the Game", "Buy this and you win!", {"win": true}))
	store.welcome_message = "Welcome, shopper! Care\nto see our newest\ndeals?"
	store.purchase_message = "Pleasure doing business\n with you."
	store.leave_message = ["Please remember to take\nyour change.", "Please remember to take\nyour receipt."]
	store.exit_message = "Have a great day!"
	store.broke_message = ["I'm afraid you can't\nafford it!", "You'll need more\ncash for that!"]
	return store

func _ready():
	random.seed = world_seed
	simplex.seed = world_seed
	simplex.period = 0.1
	random.randf()
	
	_infinite_scroll($Trees)
	_infinite_scroll($Snow)
	_infinite_scroll($Sidewalk)
	_create_snowdude(64)
		
static func round_to(s: int, step: int) -> int:
	return int(s - fposmod(s, step))
	
func try_cheat(cheat_str: String):
	if cheat_str.ends_with("QPXFSNFVQ"):
		player.strength = 99
		player.dexterity = 99
		player.constitution = 99
		player.max_hp = max(player.max_hp, 99)
		player.hp = player.max_hp
		player.gp += 10000
		player.refresh_hud()

func add_mob(mob: Node2D):
	add_child(mob)
	mobs.append(mob)

func delete_mob(mob: Node2D):
	mob.queue_free()
	mobs.erase(mob)
	
func _process(delta):
	if player != null:
		$Camera.position.x = player.position.x - VIEWPORT_WIDTH / 2
		
	var cx = $Camera.get_camera_position().x
	for pair in _scroll_pairs:
		var w = pair[0].get_rect().size.x
		pair[0].position.x = stepify(cx, w * 2)
		pair[1].position.x = stepify(cx - w, w * 2) + w

func _physics_process(delta):
	var cur_lat = player.position.x / 400.0
	if player.hud != null:
		player.hud.set_lat(cur_lat)
	max_distance = max(abs(cur_lat), max_distance)
	
	var camera_x = $Camera.get_camera_position().x
	var x_start = camera_x - MAX_SCENERY_WIDTH
	var x_end = camera_x + VIEWPORT_WIDTH
	var x_mob_start = camera_x - VIEWPORT_WIDTH * 2.0
	var x_mob_end = camera_x + VIEWPORT_WIDTH * 3.0
	var x_vis_start = round_to(x_start, SCENERY_STEP)
	var x_vis_end = round_to(x_end, SCENERY_STEP) + SCENERY_STEP * 2
	var x_despawn_start = x_vis_start - VIEWPORT_WIDTH * 2.0
	var x_despawn_end = x_vis_end + VIEWPORT_WIDTH * 2.0
	var x_mob_respawn_start = camera_x - MAX_SCENERY_WIDTH * 2
	var x_mob_respawn_end = camera_x + VIEWPORT_WIDTH + MAX_SCENERY_WIDTH * 2
	
	# prune mobs outside the view
	var _keep_mobs = []
	for mob in mobs:
		if mob.position.x < x_despawn_start or mob.position.x >= x_despawn_end:
			mob.queue_free()
		else:
			_keep_mobs.append(mob)
	mobs = _keep_mobs
	
	# prune blocked respawn spots
	for x in _cant_respawn.duplicate():
		if x < x_mob_respawn_start or x >= x_mob_respawn_end:
			_cant_respawn.erase(x)
	
	# generate/prune chunks
	var to_delete = _chunks.keys()
	for x in range(round_to(x_start, CHUNK_STEP), round_to(x_end, CHUNK_STEP) + CHUNK_STEP, CHUNK_STEP):
		to_delete.erase(x)
		if not _chunks.has(x):
			_chunks[x] = _get_visible_scenery(x, x + CHUNK_STEP)
	for x in to_delete:
		_chunks.erase(x)
	
	var building_count = 0
	var building_dist = 0
	
	# generate/prune scenery and mobs
	to_delete = _active_scenery.keys()
	var x = x_vis_start
	while x < x_vis_end:
		to_delete.erase(x)
		var cx = round_to(x, CHUNK_STEP)
		if _chunks.has(cx):
			var chunk = _chunks[cx]
			if chunk.has(x):
				if chunk[x].begins_with("building-"):
					building_count += 1
					building_dist += x - camera_x
				if not _active_scenery.has(x):
					var obj = _create_scenery_object(x, chunk[x])
					_create_mobs(x, chunk[x])
					add_child(obj)
					_active_scenery[x] = obj
				x += OBJ_WIDTHS[chunk[x]]
			else:
				x += SCENERY_STEP
		else:
			x += SCENERY_STEP
	
	for x in to_delete:
		_active_scenery[x].queue_free()
		_active_scenery.erase(x)
		
	if _walker_spawn_timer > 0:
		_walker_spawn_timer -= delta * building_count * _get_friendly_chance_norandom(camera_x)
	if _walker_spawn_timer <= 0:
		_walker_spawn_timer = rand_range(WALKER_SPAWN_INTERVAL * 0.85, WALKER_SPAWN_INTERVAL * 1.1)
		if abs(camera_x) > 400*20 and randi() % 3 == 0:
			if randi() % 2 == 0:
				_create_snowbro(x_mob_start + 16.0).aggressiveness = 1
			else:
				_create_snowbro(x_mob_end - 16.0).aggressiveness = 1
		else:
			if randi() % 2 == 0:
				_create_walker(x_mob_start + 16.0, Vector2(1, 0))
			else:
				_create_walker(x_mob_end - 16.0, Vector2(-1, 0))

func _infinite_scroll(sprite: Sprite):
	var dup : Sprite = sprite.duplicate()
	add_child(dup)
	_scroll_pairs.append([sprite, dup])

func _get_visible_scenery(x: int, x_end: int) -> Dictionary:
	var objs = {}
	x = round_to(x, SCENERY_STEP)
	x_end = round_to(x_end, SCENERY_STEP)
	while x < x_end:
		var obj_type = abs(floor(whitenoise(x, world_seed) * 20))
		
		if obj_type == 0:
			objs[x] = "building-small"
		elif obj_type == 1:
			objs[x] = "building-tall"
		elif obj_type == 2:
			objs[x] = "building-wide"
		elif obj_type <= 15:
			objs[x] = "tree"
			
		if objs.has(x):
			x += OBJ_WIDTHS[objs[x]]
		else:
			x += SCENERY_STEP
	return objs
	
const RESTAURANT_PREFIXES = ["JOE'S", "TED'S", "BETH'S", "GLORIA'S", "IRMA'S", "MONK'S", "HAPPY", "GRUMPY", "SNOWBOUND", "MAGIC", "SPICY", "CRAZY"]
const RESTAURANT_SUFFIXES = ["CAFE", "RESTAURANT", "EATERY", "DINER", "BISTRO", "SALOON", "BAR", "CAFETERIA", "DELI", "TAVERN", "PUB"]
const ARMY_PREFIXES = ["ARMY", "MILITARY", "CAVALRY", "PLATOON", "SOLDIER'S", "LEGIONNAIRE'S", "SERGEANT'S", "TROOPER'S", "GENERAL'S", "BRIGADIER'S"]
const ARMY_SUFFIXES = ["SURPLUS", "DISCOUNT", "BARGAINS", "PREMIUMS", "THRIFT", "ANTIQUES", "PAWNSHOP", "TRADING POST", "EXCHANGE", "OUTFITTERS", "BARTER", "BAZAAR"]
const SUPERSTORE_PREFIXES = ["SUPER", "MEGA", "BIG", "EXTREME", "JOHNSON", "COLOSSAL", "POPULAR", "MASSIVE", "DISCOUNT", "SUPREME", "GIGANTO"]
const SUPERSTORE_SUFFIXES = ["MART", "DEPOT", "LOTS-O-STUFF", "BARGAINS", "MEGASTORE", "WAREHOUSE", "EMPORIUM", "MALL", "MARKET", "SHOP", "WHOLESALERS"]
	
func _setup_building(x: int, building: Node):
	var random = RandomNumberGenerator.new()
	random.seed = whitenoise(x, world_seed, 654123.53) * 1048576
	random.randf()
	building.random_colors(whitenoise(x, world_seed, 625.205) * 65535.0)
	building.type = ["food", "army", "super"][random.randi() % 3]
	var words = PoolStringArray()
	match building.type:
		"food":
			words.append(RESTAURANT_PREFIXES[random.randi() % RESTAURANT_PREFIXES.size()])
			words.append(RESTAURANT_SUFFIXES[random.randi() % RESTAURANT_SUFFIXES.size()])
		"army":
			words.append(ARMY_PREFIXES[random.randi() % ARMY_PREFIXES.size()])
			words.append(ARMY_SUFFIXES[random.randi() % ARMY_SUFFIXES.size()])
		"super":
			words.append(SUPERSTORE_PREFIXES[random.randi() % SUPERSTORE_PREFIXES.size()])
			words.append(SUPERSTORE_SUFFIXES[random.randi() % SUPERSTORE_SUFFIXES.size()])
	building.set_sign_text(words.join(" "))
	building.position.x = x
	return building
	
func _get_friendly_chance_norandom(x: int) -> float:
	return 1.0 - float(x) / (400.0 * 24.0)
	
func _get_friendly_chance(x: int) -> float:
	var humanity = _get_friendly_chance_norandom(x)
	if randi() % 4 == 0:
		humanity = max(randf() * 0.2 + 0.65, humanity)
	if randi() % 8 == 0:
		humanity = 0.0
	return clamp(humanity, 0.0, 1.0)

func _create_mobs(x: int, type: String):
	if _cant_respawn.has(x):
		return
	_cant_respawn.append(x)
	var humanity: float = _get_friendly_chance(x)
	match type:
		"building-small":
			for i in range(int((randi() % 5 + 5) * humanity)):
				_create_wanderer(x + 32, x + 258 - 32)
			pass
		"building-tall":
			for i in range(int((randi() % 9 + 8) * humanity)):
				_create_wanderer(x + 32, x + 258 - 32)
			pass
		"building-wide":
			for i in range(int((randi() % 10 + 10) * humanity)):
				_create_wanderer(x + 32, x + 528 - 32)
		"tree":
			var noise = abs(floor(whitenoise(x, world_seed) * 8))
			var roll = noise * (humanity * 0.5 + 0.5)
			if roll == 2:
				_create_snowbro(x)
			elif roll == 3:
				_create_snowdude(x)
			elif roll == 4:
				_create_snowmage(x)
			elif roll == 5:
				_create_wanderer(x, x + 32)


func _create_dummy(script: Reference, params: Dictionary):
	var dummy = [preload("res://actors/DummyF.tscn"), preload("res://actors/DummyM.tscn")][params["type"]].instance()
	dummy.set_script(script)
	dummy.set_colors(params)
	return dummy

func create_player(params: Dictionary, hud: Control):
	player = _create_dummy(preload("res://scripts/Player.gd"), params)
	player.hud = hud
	player.hardcore = params["hardcore"]
	player.position.y = 186
	add_child(player)

func _create_walker(x: int, dir: Vector2):
	var npc = _create_wanderer(x, x)
	npc.stroll(dir)
	return npc

func _create_wanderer(x_start: int, x_end: int):
	var npc = _create_dummy(preload("res://scripts/AI.gd"), {"type": random.randi() % 2})
	npc.random_colors()
	return _setup_actor(npc, int(rand_range(x_start, x_end)))
	
func _create_snowbro(x: int):
	return _setup_actor(preload("res://actors/Snowbro.tscn").instance(), x)
	
func _create_snowmage(x: int):
	return _setup_actor(preload("res://actors/Snowmage.tscn").instance(), x)
	
func _create_snowdude(x: int):
	return _setup_actor(preload("res://actors/Snowdude.tscn").instance(), x)

func _setup_actor(actor: Node2D, x: int):
	actor.position = Vector2(x, random.randi() % 46 + 163)
	actor.scale.x = -1 if random.randi() % 2 else 1
	add_child(actor)
	mobs.append(actor)
	return actor

func _create_scenery_object(x: int, type: String) -> Node2D:
	match type:
		"building-small":
			return _setup_building(x, preload("res://world/BuildingSmall.tscn").instance())
		"building-tall":
			return _setup_building(x, preload("res://world/BuildingTall.tscn").instance())
		"building-wide":
			return _setup_building(x, preload("res://world/BuildingWide.tscn").instance())
		"tree":
			var tree = preload("res://world/Tree.tscn").instance()
			tree.height = abs(floor(whitenoise(x, world_seed, 352) * 32))
			tree.position.x = x + floor(whitenoise(x, world_seed, 123) * 16)
			tree.position.y = 20 - tree.height + floor(whitenoise(x, world_seed, 123) * 8)
			return tree
	return null
