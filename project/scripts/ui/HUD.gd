# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Control

onready var HPBar = $Box/HPContainer/Progress
onready var HPText = $Box/HPContainer/Counter

func set_hp(hp: int, max_hp: int):
	if HPBar.value != hp:
		var color = Color.from_hsv(1.0/3.0 * (hp/float(max_hp)), 1.0, 0.5)
		$Tween.interpolate_property(HPBar, "modulate", null, color, 0.1, Tween.TRANS_QUAD, Tween.EASE_OUT)
		$Tween.interpolate_property(HPBar, "value", null, hp, 0.1, Tween.TRANS_QUAD, Tween.EASE_OUT)
		$Tween.start()
	if HPBar.max_value != max_hp:
		var bar_size = min(5 * max_hp, 200)
		$Tween.interpolate_property(HPBar, "rect_min_size", null, Vector2(bar_size, 0), 0.1, Tween.TRANS_QUAD, Tween.EASE_OUT)
		$Tween.interpolate_property(HPBar, "max_value", null, max_hp, 0.1, Tween.TRANS_QUAD, Tween.EASE_OUT)
		$Tween.start()
	HPText.text = str(hp) + "/" + str(max_hp)

func set_gp(gp: int):
	$Upper/GP.text = str(gp)

func set_strength(val: int):
	$Box/Str.text = str(val)
	
func set_dexterity(val: int):
	$Box/Dex.text = str(val)
	
func set_constitution(val: int):
	$Box/Con.text = str(val)
	
func set_lat(lat: int):
	$Upper/Lat.text = str(lat)