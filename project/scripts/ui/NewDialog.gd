# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends CenterContainer

onready var TypeList = $NewDialog/Margins/ParamVBox/HBox/VBox/HBox/VBox/Type
onready var Hardcore = $NewDialog/Margins/ParamVBox/Params/Hardcore
onready var Seed = $NewDialog/Margins/ParamVBox/Params/Seed
onready var dummy = $NewDialog/Margins/ParamVBox/HBox/Dummy
enum ColorType { HAT, HAIR, EYES, SKIN, SHIRT, PANTS, SHOES }
onready var ColorButtons = {
	ColorType.HAT: $NewDialog/Margins/ParamVBox/HBox/VBox/HBox/Grid/Hat,
	ColorType.HAIR: $NewDialog/Margins/ParamVBox/HBox/VBox/HBox/Grid/Hair,
	ColorType.EYES: $NewDialog/Margins/ParamVBox/HBox/VBox/HBox/Grid/Eyes,
	ColorType.SKIN: $NewDialog/Margins/ParamVBox/HBox/VBox/HBox/Grid/Skin,
	ColorType.SHIRT: $NewDialog/Margins/ParamVBox/HBox/VBox/HBox/Grid/Shirt,
	ColorType.PANTS: $NewDialog/Margins/ParamVBox/HBox/VBox/HBox/Grid/Pants,
	ColorType.SHOES: $NewDialog/Margins/ParamVBox/HBox/VBox/HBox/Grid/Shoes
}

func _ready():
	randomize()
	TypeList.add_item("A")
	TypeList.add_item("B")
	randomize_dummy()
	for button in ColorButtons.values():
		var popup = button.get_popup()
		popup.connect("about_to_show", self, "_reposition", [button, popup])

func _reposition(button, popup):
	popup.rect_position = button.rect_global_position + Vector2(32, 0)

func randomize_seed():
	Seed.value = randi() % int(Seed.max_value)
	
func get_seed():
	return Seed.value

func randomize_dummy():
	dummy.random_colors()
	var values = dummy.get_colors()
	values["type"] = randi() % 2
	set_values(values)

func get_values():
	var values = dummy.get_colors()
	values["type"] = TypeList.get_selected_items()[0]
	values["hardcore"] = Hardcore.pressed
	return values

func set_values(values):
	if values.has("type"):
		TypeList.select(values["type"])
		_on_Type_item_selected(values["type"])
	if values.has("hat"):
		ColorButtons[ColorType.HAT].color = values["hat"]
	if values.has("hair"):
		ColorButtons[ColorType.HAIR].color = values["hair"]
	if values.has("eyes"):
		ColorButtons[ColorType.EYES].color = values["eyes"]
	if values.has("skin"):
		ColorButtons[ColorType.SKIN].color = values["skin"]
	if values.has("shirt"):
		ColorButtons[ColorType.SHIRT].color = values["shirt"]
	if values.has("pants"):
		ColorButtons[ColorType.PANTS].color = values["pants"]
	if values.has("shoes"):
		ColorButtons[ColorType.SHOES].color = values["shoes"]
	
func _on_Type_item_selected(index):
	var new_dummy = [preload("res://actors/DummyF.tscn"), preload("res://actors/DummyM.tscn")][index].instance()
	new_dummy.set_script(dummy.get_script())
	new_dummy.set_colors(dummy.get_colors())
	new_dummy.position = dummy.position
	new_dummy.scale = dummy.scale
	dummy.get_parent().add_child(new_dummy)
	dummy.free()
	dummy = new_dummy

func _on_Hat_color_changed(color):
	dummy.set_colors({"hat": color})

func _on_Hair_color_changed(color):
	dummy.set_colors({"hair": color})

func _on_Eyes_color_changed(color):
	dummy.set_colors({"eyes": color})

func _on_Skin_color_changed(color):
	dummy.set_colors({"skin": color})

func _on_Shirt_color_changed(color):
	dummy.set_colors({"shirt": color})
	
func _on_Pants_color_changed(color):
	dummy.set_colors({"pants": color})

func _on_Shoes_color_changed(color):
	dummy.set_colors({"shoes": color})
	
func _on_RandomizeButton_pressed():
	randomize_dummy()
