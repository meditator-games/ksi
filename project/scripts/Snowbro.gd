# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends KinematicBody2D

const MOVE_SPEED_Y = 40.0
const MOVE_SPEED_X = 60.0
const FIND_TARGET_TICK = 1.0
const THROW_DISTANCE = 64.0
const ATTACK_DISTANCE = 16.0
const ATTACK_DISTANCE_Y = 4.0
const ATTACK_DURATION = 0.2
const HITBOX_DEPTH = 10.0
const KICK_THRUST = 2.0

var _target_mob_path = null
var _target_position = null
var _find_target_timer = 0
var aggressiveness = 0.0
var anim_timer = 0
var laugh_timer = 0
var kick_timer = 0
var throw_timer = 0
var legs_timer = 0
var hurt_timer = 0
var blink_timer = 0
var death_timer = 0
var laugh_subpixel = 0
var force_throw = false
var vel: Vector2 = Vector2()

var hp: int = -1
var strength: int = 1

func _ready():
	$AreaBody.set_meta("body", self)
	set_meta("top", $TopPoint)
	if hp == -1:
		var interval = abs(position.x) / 400.0
		hp = floor(clamp(interval, 6.0, 200.0))
		strength = floor(clamp(interval * 1.1, 1, 99))
		aggressiveness = floor(clamp(interval * 0.16, 0, 1))


func _process(delta):
	z_index = int(position.y)
	
	if laugh_timer > 0:
		laugh_timer -= delta
		if $Body/Face.animation == "Normal":
			laugh_subpixel = (laugh_subpixel + 1) % 2
			if laugh_subpixel == 0:
				$Body/Face.offset.y = -1 if $Body/Face.offset.y == 0 else 0
		else:
			$Body/Face.offset.y = 0
	else:
		$Body/Face.offset.y = 0
		
	if hp <= 0:
		death_timer += delta
		if death_timer > 0.5:
			visible = !visible
		if death_timer > 1.0:
			get_tree().call_group("world", "delete_mob", self)
			ActorFuncs.spawn_gold(get_tree(), self.position, clamp(strength * 0.75, 1, 10))
	
func _find_new_target(agg):
	if randf() < agg:
		var target_mob = ActorFuncs.find_target(get_parent(), agg)
		if target_mob != null:
			_target_mob_path = target_mob.get_path()
			return target_mob
	return null
	
func _physics_process(delta):
	if hp > 0:
		var target_mob = get_node(_target_mob_path) if _target_mob_path != null and has_node(_target_mob_path) else null
		
		if kick_timer > 0:
			kick_timer -= delta
			if $Legs.animation != "Kick":
				$Legs.animation = "Kick"
				$AreaKick.monitoring = true
			else:
				$AreaKick.monitoring = false
			if kick_timer <= 0:
				force_throw = randi() % 3 == 0
				if not force_throw and randf() < 0.1 - aggressiveness * 0.1:
					_target_mob_path = null
		elif throw_timer > 0:
			force_throw = false
			throw_timer -= delta
			if throw_timer > 0:
				$Body/Arms.animation = "Throw"
			else:
				if target_mob != null:
					_throw_snowball_at(target_mob.position)
				else:
					_throw_snowball_at(position + Vector2(-scale.x, 0))
				$Body/Arms.animation = "Normal"
				if randi() % 2 == 0:
					laugh_timer = 0.5
					$Laugh.pitch_scale = 1.0
					$Laugh.play()
				if randf() < 0.1 - aggressiveness * 0.1:
					_target_mob_path = null
		elif hurt_timer <= 0:
			# try to find a target
			if target_mob == null:
				_find_target_timer -= delta
				if _find_target_timer <= 0:
					if randf() < aggressiveness:
						target_mob = ActorFuncs.find_target(get_parent(), aggressiveness)
						if target_mob != null:
							_target_mob_path = target_mob.get_path()
					else:
						scale.x *= -1
						_find_target_timer = FIND_TARGET_TICK
			
			if target_mob == null:
				anim_timer = fmod(anim_timer + delta, 0.9)
				$Body.position.y = floor((anim_timer / 0.9) * 2.0)
			else:
				_chase(target_mob, delta)
		
	# XY movement
	var collision: KinematicCollision2D = move_and_collide(Vector2(vel.x, vel.y), true, true, true)
	if collision != null:
		vel = collision.travel
		if collision.collider is preload("res://scripts/Dummy.gd"):
			if kick_timer <= 0:
				if collision.collider.position.x < position.x:
					scale.x = 1
				else:
					scale.x = -1
				_start_kick()
		else:
			_target_position = null
	position += vel
	
	if hp > 0 and hurt_timer <= 0 and throw_timer <= 0 and kick_timer <= 0:
		if abs(vel.x) > 0.001 or abs(vel.y) > 0.001:
				legs_timer = fmod(legs_timer + delta * vel.length() * 2.0, 1.0)
				$Legs.animation = ["Walk1", "Walk2", "Walk3", "Walk2"][floor(legs_timer * 4)]
				$Step.playing = true
		else:
			$Step.playing = false
			$Legs.animation = "Normal"
			legs_timer = 0
		ActorFuncs.blink(self, $Body/Face, delta)
	else:
		$Step.playing = false
	
	if hurt_timer > 0:
		hurt_timer -= delta
	
	vel.x *= 1 - delta * 6    # friction
	vel.y *= 1 - delta * 6
	
func is_dead():
	return hp <= 0
	
func damaged_melee(source: Node2D, dir: Vector3, damage: int):
	if hurt_timer > 0 or hp <= 0:
		return
	hp -= damage
	hurt_timer = 0.5
	laugh_timer = 0
	vel = Vector2(dir.x, dir.y)
	$Legs.animation = "Walk2"
	$Body/Arms.animation = "Normal"
	if hp > 0:
		$Body/Face.animation = "Hurt"
		if source.has_meta("owner_path"):
			_target_mob_path = source.get_meta("owner_path")
		else:
			_target_mob_path = source.get_path()
		aggressiveness += 0.1
	else:
		$Body/Face.animation = "Dead"

func _throw_snowball_at(target_pos: Vector2) -> Node2D:
	$Throw.play()
	var snowball = preload("res://actors/Snowball.tscn").instance()
	snowball.position = Vector2($SnowballSpawn.global_position.x, position.y)
	snowball.set_z(-$SnowballSpawn.position.y)
	snowball.direction = (target_pos - position).normalized()
	snowball.damage = clamp(strength / 2, 1, 9999)
	snowball.speed = 1.0 + aggressiveness * 0.5
	snowball.set_meta("owner_path", get_path())
	get_tree().call_group("world", "add_mob", snowball)
	return snowball

func _start_kick():
	vel = Vector2()
	kick_timer = ATTACK_DURATION
	_target_position = null

func _chase(target_mob, delta):
	if target_mob == null or target_mob.is_dead():
		target_mob = null
		_target_mob_path = null
		_target_position = null
		_find_target_timer = FIND_TARGET_TICK
		return
		
	$Body.position.y = 0
	var dist_to_target = position.distance_to(target_mob.position)
	var dir = sign(position.x - target_mob.position.x)
	if dir == 0:
		dir = 1
		
	# attack table
	if not force_throw:
		if abs(target_mob.position.x - position.x) < ATTACK_DISTANCE and abs(target_mob.position.y - position.y) < ATTACK_DISTANCE_Y:
			_start_kick()
		elif dist_to_target < THROW_DISTANCE:
			_target_position = target_mob.position + Vector2((ATTACK_DISTANCE - 2.0) * dir, 0)
		elif _target_position == null:
			force_throw = true
			
	if force_throw and _target_position == null:
		_target_position = target_mob.position + Vector2(rand_range(120, 160) * dir, 0)
	
	# move in
	if _target_position != null:
		var move_speed = 1.0 + aggressiveness
		var move_max = Vector2(MOVE_SPEED_X, MOVE_SPEED_Y) * move_speed * delta
		var move_vec = _target_position - position
			
		if move_vec.x < 0:
			scale.x = 1
		elif move_vec.x > 0:
			scale.x = -1
			
		if move_vec.length() > move_max.length():
			var move_dir = move_vec.normalized()
			vel = move_dir * move_max
		else:
			#vel = move_vec
			var final_dist = position.distance_to(target_mob.position)
			if final_dist < ATTACK_DISTANCE:
				kick_timer = ATTACK_DURATION
			else:
				throw_timer = ATTACK_DURATION
			scale.x = sign(position.x - target_mob.position.x)
			if scale.x == 0:
				scale.x = 1
			vel = Vector2()
			_target_position = null

func _on_AreaKick_area_entered(area):
	if ActorFuncs.melee_attack(self, area, KICK_THRUST, strength) != null:
		$Kick.play()
		if randi() % 2 == 0:
			laugh_timer = 0.2
			$Laugh.pitch_scale = 1.5
			$Laugh.play()
