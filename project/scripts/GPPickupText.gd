# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Sprite

const DURATION = 1.0
const MOVE_SPEED = 12.0

var value: int = 0
var _timer = DURATION

func _ready():
	if value >= 0:
		$Viewport/Label.text = "+" + str(value) + "GP"
	else:
		$Viewport/Label.text = str(value) + "GP"

func _process(delta):
	_timer -= delta
	position.y -= delta * MOVE_SPEED
	modulate.a = _timer / DURATION
	if _timer <= 0:
		queue_free()