# This file is part of Killer Snowman Invasion.
#
# Killer Snowman Invasion is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# Killer Snowman Invasion is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Killer Snowman Invasion.  If not, see
# <https://www.gnu.org/licenses/>.

extends Sprite

onready var Label = $Viewport/VBoxContainer/Box/Label

var text_subcounter: float = 0
const CHAR_SPEED = 0.02
const FADE_DURATION = 0.1
var closing: bool = false
var owner_path = null

func _ready():
	modulate = Color(1.0, 1.0, 1.0, 0.0)

func _process(delta):
	if not closing and modulate.a == 1.0 and Label.visible_characters < Label.text.length():
		text_subcounter += delta
		while text_subcounter > CHAR_SPEED and Label.visible_characters < Label.text.length():
			Label.visible_characters += 1
			text_subcounter -= CHAR_SPEED

func _physics_process(delta):
	if owner_path != null and has_node(owner_path):
		var owner = get_node(owner_path)
		var target = owner.get_meta("top") if owner.has_meta("top") else owner
		position = target.global_position - Vector2($Viewport.size.x / 2, $Viewport.size.y + 8)
		z_index = owner.z_index

func set_size(size: Vector2):
	$Viewport.size = size

func say(text: String, owner: Node):
	$CloseTimer.stop()
	owner_path = owner.get_path()
	owner.set_meta("bubble", self)
	Label.text = text
	text_subcounter = 0
	$Fade.stop_all()
	$Fade.remove_all()
	$Fade.interpolate_property(self, "modulate", null, Color(1.0, 1.0, 1.0, 1.0), FADE_DURATION, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Fade.start()
	
func get_time_left() -> float:
	if closing:
		return 0.0
	return CHAR_SPEED * (Label.text.length() - Label.visible_characters)
	
func close():
	if owner_path != null and has_node(owner_path):
		get_node(owner_path).set_meta("bubble", null)
	closing = true
	Label.text = ""
	Label.visible_characters = 0
	$Fade.stop_all()
	$Fade.remove_all()
	$Fade.interpolate_property(self, "modulate", null, Color(1.0, 1.0, 1.0, 0.0), FADE_DURATION, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Fade.start()
	
func close_after(secs: float = 0.0):
	$CloseTimer.start(secs + get_time_left())

func _on_Tween_completed(object, key):
	if closing:
		queue_free()
