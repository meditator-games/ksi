def select_kernel(image, kernel):
    image.undo_group_start()
    sel = image.selection
    data = sel.get_data()
    for y in range(image.height):
        for x in range(image.width):
            p = int(data[(y * image.width + x) * 4] * 255)
            if p == 0:
                continue
            for k in kernel:
                pos = (x + k[0], y + k[1])
                if pos[0] >= 0 and pos[0] < image.width and pos[1] >= 0 and pos[1] < image.height:
                    if int(data[(pos[1] * image.width + pos[0]) * 4] * 255) == 0:
                        sel.set_pixel(pos[0], pos[1], (255, ))
    image.undo_group_end()

# select_kernel(gimp.image_list()[0], [(0, 1), (0, -1), (1, 0), (-1, 0)])
